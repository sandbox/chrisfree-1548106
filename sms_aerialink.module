<?php
/**
 * @file
 * Aerialink gateway module for Drupal SMS Framework.
 *
 * @package sms
 * @subpackage sms_aerialink
 */

/**
 * Implementation hook_gateway_info()
 *
 * @ingroup hooks
 */
function sms_aerialink_gateway_info() {
  return array(
    'aerialinkgateway' => array(
      'name'           => 'Aerialink Gateway',
      'send'           => 'sms_aerialink_send',
      'configure form' => 'sms_aerialink_admin_form',
    ),
  );
}

/**
 * Configuration form callback.
 *
 * @param $configuration
 *   Drupal admin form configuration array.
 *
 * @return
 *   Drupal FAPI form array.
 */
function sms_aerialink_admin_form($configuration) {
  $form['sms_aerialink_send'] = array(
    '#type' => 'fieldset',
    '#title' => 'Aerialink API Keys',
  );

  $form['sms_aerialink_send']['sms_aerialink_api_path'] = array(
    '#type' => 'textfield',
    '#title' => t('API Path'),
    '#description' => t('Eg: api.aerialink.net/20100101/sms/submit/, NO http:// and INCLUDES a trailing slash'),
    '#size' => 60,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => $configuration['sms_aerialink_api_path'],
  );

  $form['sms_aerialink_send']['sms_aerialink_sid_value'] = array(
    '#type' => 'textfield',
    '#title' => t('SID / Username'),
    '#description' => t('SID for Aerialink'),
    '#size' => 40,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => $configuration['sms_aerialink_sid_value'],
  );

  $form['sms_aerialink_send']['sms_aerialink_token_value'] = array(
    '#type' => 'textfield',
    '#title' => t('Token / Password'),
    '#description' => t('Token for Aerialink'),
    '#size' => 40,
    '#maxlength' => 255,
    '#required' => TRUE,
    '#default_value' => $configuration['sms_aerialink_token_value'],
  );

  return $form;
}

/**
 * Send a message
 *
 * @param $number
 *   MSISDN of message recipient. Expected to include the country code prefix.
 * @param $message
 *   Message body text.
 * @param $options
 *   Options array from SMS Framework.
 *
 * @return
 *   Response array.
 */
function sms_aerialink_send($number, $message, $options) {

  $gateway = sms_gateways('gateway', 'aerialinkgateway');
  $config = $gateway['configuration'];

  $sid = $config['sms_aerialink_sid_value'];
  $token = $config['sms_aerialink_token_value'];
  $api_path = $config['sms_aerialink_api_path'];

  $data = array();
  $data["destination"] = $number;
  $data["messageText"] = $message;

  $query_string = http_build_query($data, NULL, '&');

  // Builds the URL for drupal_http_request to use in REST API call.
  $build_url = 'http://' . $sid . ':' . $token . '@' . $api_path;
  $headers = array('Content-Type' => 'application/x-www-form-urlencoded');

  $http_result = drupal_http_request($build_url, $headers, 'POST', $query_string);

  // Check for HTTP errors.
  if ($http_result->error) {
    return array(
      'status'  => FALSE,
      'message' => t('An error occured during the HTTP request: @error',
                    array('@error' => $http_result->error)),
    );
  }

  if ($http_result->data) {
    // Test the HTTP return code.
    if ($http_result->code >= 200 && $http_result->code <= 299) {
      if (strpos($http_result->data, 'Error') === FALSE) {
        // Prepare a good response array.
        $result = array(
          'status' => TRUE,
          'message' => $http_result->data,
        );
      }
      else {
        // We got a (possibly) bad response code.
        $result = array(
          'status' => FALSE,
          'message' => $http_result->data,
        );
      }
    }
  }
  return $result;
}

function sms_aerialink_disable() {
  cache_clear_all('services:methods', 'cache');
}

function sms_aerialink_enable() {
  cache_clear_all('services:methods', 'cache');
}

/**
 * Implementation of hook_service().
 */
function sms_aerialink_service() {
  return array(
    array(
      '#method' => 'sms_aerialink.receive',
      '#callback' => 'sms_aerialink_receive',
      '#access callback' => 'sms_aerialink_service_access_test',
      '#auth' => FALSE,
      '#return' => 'struct',
      '#key' => FALSE,
      '#args' => array(),
      '#help' => t('Returns an object containing a sessid and user.')));
}

/**
 * Custom access callback for SMS Service.
 */
function sms_aerialink_service_access_test() {
  if (!isset($_SERVER['PHP_AUTH_USER'])) {
    header('WWW-Authenticate: Basic realm="API Access"');
    return FALSE;
  }
  else {
    if ($_SERVER['PHP_AUTH_USER'] === variable_get('sms_api_user', FALSE) && $_SERVER['PHP_AUTH_PW'] === variable_get('sms_api_password', FALSE)) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Custom SMS service callback.
 */
function sms_aerialink_receive() {
  $number = filter_xss($_POST['source']); // the incoming messages phone number.
  $message_text = filter_xss($_POST['messageText']); // the incoming message text.

  // Passes incoming messages into the SMS Framework
  sms_incoming($number, $message_text);
}
